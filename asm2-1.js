var data = [{
    name: "Phạm Văn A",
    math: 5,
    physical: 6,
    chemistry: 7
},
{
    name: "Trần Thị B",
    math: 8,
    physical: 9,
    chemistry: 9
}
];

var testNumber = /^[0-9.-]+$/
var displayAverage = false;
var showGoodStudent = false;

function makeColum(row, textnode, rownumber, colnumber) {
    var col = document.createElement("td");
    row.appendChild(col);
    col.appendChild(document.createTextNode(textnode));
    col.id = "colScoreTable-" + rownumber + "-" + colnumber;

}

//make main row
function makeRow(row, num) {

    var col = document.createElement("td");
    row.appendChild(col);
    col.innerHTML = "<input type='checkbox' name = 'student'/>";
    col.id = "colScoreTable-" + num + "-0";

    makeColum(row, num, num, 1);
    makeColum(row, data[num - 1].name, num, 2);
    makeColum(row, data[num - 1].math, num, 3);
    makeColum(row, data[num - 1].physical, num, 4);
    makeColum(row, data[num - 1].chemistry, num, 5);
}

//load all elements already in the data

function loadData() {

    var table = document.getElementById("result");

    for (var i = 1; i <= data.length; i++) {

        var row = document.createElement("TR");
        row.id = "rowScoreTable" + i;

        table.appendChild(row);
        makeRow(row, i);

        if (!displayAverage) {
            makeColum(row, "?", i, 6);
        } else {
            makeColum(row, average(i), i, 6);
        }

        if (showGoodStudent) {
            displayGoodStudents();
        }
    }

}

//add a element to data

function insertData() {


    var testScore = {
        name: "",
        math: 0,
        physical: 0,
        chemistry: 0
    };

    testScore.name = document.getElementById("name").value;
    testScore.math = document.getElementById("math").value;
    testScore.physical = document.getElementById("physical").value;
    testScore.chemistry = document.getElementById("chemistry").value;

    if (!testNumber.test(testScore.math) || !testNumber.test(testScore.physical) || !testNumber.test(testScore.chemistry)) {
        alert("nhập điểm không hợp lệ");
        return false;
    }
    if (testScore.math > 10 || testScore.math < 0 || testScore.physical > 10 || testScore.physical < 0 || testScore.chemistry > 10 ||
        testScore.chemistry < 0) {

        alert("Điểm Toán Lý Hóa lớn hơn hoặc bằng 0 và nhỏ hơn hoặc bằng 10");
        return false;
    }

    displayAverage = false;
    showGoodStudent = false;

    data.push(testScore);
    document.getElementById("name").value = "";
    document.getElementById("math").value = "";
    document.getElementById("physical").value = "";
    document.getElementById("chemistry").value = "";

    var table = document.getElementById("result");
    var row = document.createElement("TR");
    table.appendChild(row);
    row.id = "rowScoreTable" + data.length;

    makeRow(row, data.length);
    makeColum(row, "?", data.length, 6);

    return true;

}

//caculate average score

function average(num) {
    var sum = (parseFloat(data[num - 1].math) + parseFloat(data[num - 1].physical) + parseFloat(data[num - 1].chemistry)) / 3;
    var average = sum.toFixed(1);
    return average;
}


// display average score

function averageScore() {

    displayAverage = true;

    for (var i = 1; i <= data.length; i++) {
        var idCol = "colScoreTable-" + i + "-" + 6;
        document.getElementById(idCol).innerHTML = average(i);

    }
}

// display Good Students

function displayGoodStudents() {

    for (var i = 1; i <= data.length; i++) {

        var idCol = "colScoreTable-" + i + "-" + 6;
        var idRow = "rowScoreTable" + i;
        if (document.getElementById(idCol).innerHTML == "?") {
            alert("Bạn chưa tính hết điểm trung bình của tất cả học sinh.");
            break;
        } else {
            if (average(i) >= 8) {
                showGoodStudent = true;
                document.getElementById(idRow).style.color = "red";
            }
        }
    }
}

// All checkbox checked

function checkedAll() {

    var checkbox = document.getElementsByName("student");
    if (checkbox[0].checked) {
        for (var i = 1; i <= data.length; i++) {
            checkbox[i].checked = true;
        }
    } else {
        for (var i = 1; i <= data.length; i++) {
            checkbox[i].checked = false;
        }
    }
}

//repair Student

function repairStudent() {

    var checkbox = document.getElementsByName("student");
    var check = false;
    for (var i = 1; i <= data.length; i++) {
        if (checkbox[i].checked) {

            check = true;

            for (var j = 2; j < 6; j++) {

                var idCol = "colScoreTable-" + i + "-" + j;
                var cell = document.getElementById(idCol);
                var val = cell.innerHTML;

                cell.innerHTML = "";
                var textbox = document.createElement("input");
                cell.appendChild(textbox);
                textbox.type = "text";
                textbox.value = val;
                textbox.id = "textboxStudent-" + i + "-" + j;

            }

        }
    }
    if (check) {
        document.getElementById("repair-student").style.display = "none";
        document.getElementById("repair-student-success").style.display = "block";
    } else {
        alert("Chọn học sinh mà bạn muốn sửa");
    }

}

//repair Student success

function repairStudentSuccess() {
    var checkbox = document.getElementsByName("student");
    var check = false;
    for (var i = 1; i <= data.length; i++) {

        if (checkbox[i].checked) {

            check = true;

            var testScore = {
                name: "",
                math: 0,
                physical: 0,
                chemistry: 0
            };

            for (var j = 2; j < 6; j++) {

                var idCol = "colScoreTable-" + i + "-" + j;
                var cell = document.getElementById(idCol);

                var idTextBox = "textboxStudent-" + i + "-" + j;
                var textbox = document.getElementById(idTextBox);
                var val = textbox.value;

                if (j != 2) {

                    if (!testNumber.test(val)) {

                        alert("Điểm ở dòng " + i + " phải là số");
                        break;
                    }

                    if (val > 10 || val < 0) {
                        alert("Điểm ở dòng " + i + " phải lớn hơn hoặc bằng 0 và nhỏ hơn hoặc bằng 10");
                        break;
                    }
                } else {
                    testScore.name = val;
                }
                if (j == 3) {
                    testScore.math = val;
                }
                if (j == 4) {
                    testScore.physical = val;
                }
                if (j == 5) {
                    testScore.chemistry = val;
                }
                cell.innerHTML = val;
            }

            data[i - 1] = testScore;
            var idCol = "colScoreTable-" + i + "-6";
            var cell = document.getElementById(idCol);
            cell.innerHTML = "?";
            displayAverage = false;
            showGoodStudent = false;
        }

    }
    if (check) {
        document.getElementById("repair-student").style.display = "block";
        document.getElementById("repair-student-success").style.display = "none";
    }
}


//delete a student in data 

function deleteStudent() {

    var checkbox = document.getElementsByName("student");

    var check = false;
    var deleteArray = [];

    for (var i = 1; i <= data.length; i++) {

        if (checkbox[i].checked) {
            check = true;
            deleteArray.push(i - 1);

        }
    }

    for (var i = 0; i < deleteArray.length; i++) {
        data.splice(deleteArray[i], 1);
        
    }

    document.getElementById("result").innerHTML = " <tr><th><input type='checkbox' name='student' onclick='checkedAll()'/>"
        + "</th><th>STT</th><th>Họ tên</th><th>Toán</th><th>Lý</th><th>Hóa</th><th>Điểm trung bình</th></tr>";
    loadData();

    if (!check) {
        alert("Chọn học sinh mà bạn muốn xóa");
    }
}
